import React, { memo } from "react";
import * as R from "ramda";
import { Snackbar } from "@material-ui/core";
import {  FormikErrors } from "formik";

interface SnackbarProps {
  errors: FormikErrors<any>;
  setErrors: (errors: FormikErrors<any>) => void;
}

export default memo((props: SnackbarProps) => {
  const { errors, setErrors } = props;
  console.log("render warning");
  return (
    <Snackbar
      style={{ whiteSpace: "pre" }}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
      open={!R.isEmpty(errors)}
      onClose={() => setErrors({})}
      message={Object.values(errors)
        .map(val => val)
        .join("\n")}
    />
  );
});
