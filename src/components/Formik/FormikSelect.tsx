import React from "react";
import {
  Select,
  SelectProps,
  InputLabel,
  FormControl
} from "@material-ui/core";
import { FieldProps } from "formik";

interface FormikSelectProps extends FieldProps {
  style?: React.CSSProperties;
  label?: string;
}

const fieldToSelect = ({
  field: { name, value, onChange },
  ...props
}: FormikSelectProps): SelectProps => {
  return {
    ...props,
    name,
    value,
    onChange,
    style: {}
  };
};

export default ({ style, ...props }: FormikSelectProps) => {
  const { label } = props;
  return (
    <FormControl variant="outlined" style={style}>
      <InputLabel style={{}}>{label}</InputLabel>
      <Select {...fieldToSelect(props)} />
    </FormControl>
  );
};
