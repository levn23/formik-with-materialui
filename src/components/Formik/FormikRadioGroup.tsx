import React from "react";
import {
  RadioGroupProps,
  FormControl,
  FormLabel,
  RadioGroup
} from "@material-ui/core";
import { FieldProps } from "formik";

interface FormikRadioGroupProps extends FieldProps {
  style?: React.CSSProperties;
  legend?: string;
}

const fieldToRadioGroup = ({
  field: { name, value, onChange },
  form,
  ...props
}: FormikRadioGroupProps): RadioGroupProps => {
  return {
    ...props,
    name,
    value,
    onChange
  };
};

export default ({ style, legend, ...props }: FormikRadioGroupProps) => {
  return (
    <FormControl component="fieldset" style={style}>
      <FormLabel component="legend">{legend}</FormLabel>
      <RadioGroup {...fieldToRadioGroup(props)} />
    </FormControl>
  );
};
