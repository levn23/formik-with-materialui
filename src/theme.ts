import { createMuiTheme } from "@material-ui/core/styles";
import { red } from "@material-ui/core/colors";

export default createMuiTheme({
  props: {
    MuiButton: {
      size: "small",
      variant: "contained"
    },
    MuiIconButton: {
      size: "small"
    },
    MuiTextField: {
      variant: "outlined"
    },
    MuiCheckbox: {
      color: "primary"
    },
    MuiSelect: {
      variant: "outlined"
    },
    MuiRadio: {
      color: "primary"
    }
  },
  palette: {
    primary: {
      main: "#1ba1e2"
    },
    error: {
      main: red.A400
    },
    background: {
      default: "#fff"
    }
  },
  overrides: {
    // grid
    MuiGrid: {
      item: {
        padding: "4px"
      }
    },
    // form control
    MuiFormControlLabel: {
      root: {
        marginRight: "10px"
      }
    },
    MuiFormLabel: {
      root: {
        color: "#888888",
        "&$disabled": {
          color: "rgba(0, 0, 0, 0.54)"
        }
      }
    },
    // tooltip
    MuiTooltip: {
      tooltip: {
        fontSize: "1em"
      }
    },
    // table
    MuiTable: {
      root: {
        borderCollapse: "separate"
      }
    },
    MuiTableCell: {
      head: {
        whiteSpace: "pre-wrap",
        "&:first-of-type": { borderLeft: "1px solid #e0e0e0" },
        borderTop: "1px solid #e0e0e0",
        borderRight: "1px solid #e0e0e0"
      },
      body: {
        wordBreak: "break-word",
        lineHeight: "1.1em",
        padding: "5px",
        "&:first-of-type": { borderLeft: "1px solid #e0e0e0" },
        borderRight: "1px solid #e0e0e0",
        borderBottom: "1px solid #e0e0e0"
      },
      sizeSmall: {
        lineHeight: "1.1em",
        padding: "5px"
      }
    },
    // button
    MuiButton: {
      root: {
        margin: "0 4px"
      },
      containedPrimary: {
        color: "white"
      }
    },
    MuiIconButton: {
      root: {
        padding: "4px"
      }
    },
    // input
    MuiInputLabel: {
      outlined: {
        transform: "translate(14px, 8px) scale(1)"
      }
    },
    MuiOutlinedInput: {
      root: {
        "&$disabled": {
          color: "#000",
          backgroundColor: "#f0f0f0"
        },
        "&$error  $notchedOutline": {
          borderColor: red.A400
        }
      },
      input: {
        padding: "7px 11px 6px 11px"
      },
      multiline: {
        padding: "9px 11px"
      },
      adornedEnd: {
        paddingRight: "10px"
      }
    },
    // checkbox
    MuiCheckbox: {
      root: {
        padding: "5px"
      }
    },
    // typography
    MuiTypography: {
      body1: {
        fontSize: "0.9rem",
        lineHeight: 1
      }
    },
    // dialog
    MuiDialogTitle: {
      root: {
        padding: "8px 24px"
      }
    },
    MuiDialogContent: {
      root: {
        padding: "16px 12px"
      },
      dividers: {
        padding: "16px 12px"
      }
    },
    MuiDialogActions: {
      root: {
        padding: "8px 8px"
      }
    }
  }
});
