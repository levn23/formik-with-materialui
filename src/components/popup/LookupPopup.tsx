import React, { useState, memo } from "react";
import {
  Grid,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle
} from "@material-ui/core";
import MaterialTable from "material-table";

export interface LookupPopupProp {
  columns?: any[];
  data?: any[];
  okHandler?: (selectedItem: any) => void;
  cancelHandler?: () => void;
}

export default memo((props: LookupPopupProp) => {
  const { columns, data, okHandler, cancelHandler } = props;
  const [selectedRow, setSelectedRow] = useState<any>(null);
  console.log("render lookup");
  return (
    <Dialog
      open={!!columns}
      maxWidth={false}
      aria-labelledby="lookupPopupTitle"
    >
      <DialogTitle id="lookupPopupTitle">Please select</DialogTitle>
      <DialogContent dividers>
        <Grid container spacing={1}>
          <MaterialTable
            title={`Total: ${data?.length} row(s)`}
            columns={columns!}
            data={data!}
            onRowClick={(event: any, rowData: any) =>
              rowData === selectedRow
                ? setSelectedRow(null)
                : setSelectedRow(rowData)
            }
            options={{
              draggable: false,
              toolbar: true,
              search: false,
              padding: "dense",
              paging: false,
              maxBodyHeight: 250,
              minBodyHeight: 250,
              rowStyle: (rowData: any) =>
                selectedRow && selectedRow.tableData.id === rowData.tableData.id
                  ? {
                      backgroundColor: "#ddd"
                    }
                  : {}
            }}
          />
        </Grid>
      </DialogContent>

      <DialogActions>
        <Button
          onClick={() => !!okHandler && okHandler(selectedRow)}
          color="primary"
          disabled={selectedRow === null}
        >
          OK
        </Button>
        <Button
          onClick={() => !!cancelHandler && cancelHandler()}
          color="default"
        >
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
});
