import XlsxPopulate from "xlsx-populate";
import FileSaver from "file-saver";

export interface Column {
  title: string;
  key: string;
  width?: number;
}

export interface Row {
  [key: string]: any;
}

export interface ExportProps {
  columns: Column[];
  data: Row[];
  fileName: string;
  password?: string;
}

const generateExcel = (columns: Column[], data: Row[], password?: string) => {
  return XlsxPopulate.fromBlankAsync().then(workbook => {
    const sheet = workbook.sheet(0);

    columns.forEach((col, colIdx) => {
      // header
      if (col.width) {
        sheet.column(colIdx + 1).width(col.width);
      }
      sheet
        .row(1)
        .cell(colIdx + 1)
        .style("bold", true)
        .value(col.title);

      //body
      data.forEach((row, rowIdx) => {
        const val = row[col.key];

        const cell = sheet
          .row(rowIdx + 2)
          .cell(colIdx + 1)
          .value(val);
        if (val instanceof Date) {
          cell.style("numberFormat", "dd-MMM-yyyy");
        }
      });
    });
    return workbook.outputAsync({ password });
  });
};

export const exportExcel = ({
  columns,
  data,
  fileName,
  password
}: ExportProps) => {
  generateExcel(columns, data, password)
    .then(blob => {
      FileSaver.saveAs(blob as Blob, fileName + ".xlsx");
    })
    .catch(err => {
      alert(err.message || err);
      throw err;
    });
};

export const readExcel = (file: File) => {
  return XlsxPopulate.fromDataAsync(file)
    .then(workbook => {
      const sheet = workbook.sheet(0);
      if (sheet) {
        const rows = sheet.usedRange()?.value();
        return rows;
      }
      return [];
    })
    .catch(err => {
      alert(err.message || err);
    });
};
