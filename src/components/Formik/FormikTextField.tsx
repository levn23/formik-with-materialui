import React, { forwardRef } from "react";
import {
  TextField,
  InputAdornment,
  IconButton,
  SvgIconTypeMap,
  SvgIcon,
  TextFieldProps
} from "@material-ui/core";
import { OverridableComponent } from "@material-ui/core/OverridableComponent";
import { FieldProps, getIn } from "formik";
import SearchIcon from "@material-ui/icons/Search"

interface FormikTextFieldProps extends FieldProps {
  uppercase?: boolean;
  lookupIcon?: OverridableComponent<SvgIconTypeMap<{}, "svg">>;
  lookupOnClick?: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void;
}

const fieldToTextField = ({
  uppercase,
  lookupIcon,
  lookupOnClick,
  field: { name, value },
  form: { errors, setFieldValue },
  ...props
}: FormikTextFieldProps): TextFieldProps => {
  const fieldError = getIn(errors, name);

  const InputProps = lookupOnClick && {
    endAdornment: (
      <InputAdornment position="end">
        <IconButton edge="end" onClick={lookupOnClick}>
          <SvgIcon component={lookupIcon ? lookupIcon : SearchIcon} />
        </IconButton>
      </InputAdornment>
    )
  };
  return {
    ...props,
    name,
    value,
    onChange: (event: any) => {
      const text = uppercase
        ? event.target.value.toUpperCase()
        : event.target.value;
      setFieldValue(name, text);
    },
    error: Boolean(fieldError),
    InputProps
  };
};

export default forwardRef((props: FormikTextFieldProps, ref: any) => {
  console.log("render textfield")
  return (
    <TextField
      {...fieldToTextField(props)}
      variant="outlined"
      ref={ref}
      autoComplete="off"
    />
  );
});
