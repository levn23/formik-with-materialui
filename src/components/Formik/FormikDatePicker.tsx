import React from "react";

import {
  KeyboardDatePicker,
  KeyboardDatePickerProps
} from "@material-ui/pickers";
import { FieldProps, getIn } from "formik";

interface FormikDatePickerProps extends FieldProps {}

const fieldToKeyboardDatePicker = ({
  field: { name, value },
  form: { errors, setFieldValue },
  ...props
}: FormikDatePickerProps): KeyboardDatePickerProps => {
  const fieldError = getIn(errors, name);
  const data = {
    ...props,
    name,
    value,
    onChange: (date: any) => {
      setFieldValue(name, date);
    }
  };

  return fieldError === undefined
    ? data
    : { ...data, error: Boolean(fieldError) };
};

export default ({ ...props }: FormikDatePickerProps) => {
  return (
    <KeyboardDatePicker
      {...fieldToKeyboardDatePicker(props)}
      format="dd/MM/yyyy"
      autoOk
      variant="inline"
      inputVariant="outlined"
      autoComplete="off"
      KeyboardButtonProps={{ size: "small" }}
    />
  );
};
