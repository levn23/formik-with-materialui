import React, { useState } from "react";
import "./App.css";
import { Formik, FormikErrors, Form, FastField } from "formik";
import moment from "moment-timezone";
import { FormControlLabel, Radio, MenuItem, Button } from "@material-ui/core";
import * as Yup from "yup";
import * as R from "ramda";
import FormikDatePicker from "./components/formik/FormikDatePicker";
import FormikRadioGroup from "./components/formik/FormikRadioGroup";
import FormikCheckbox from "./components/formik/FormikCheckbox";
import FormikSelect from "./components/formik/FormikSelect";
import FormikTextField from "./components/formik/FormikTextField";
import LookupPopup, { LookupPopupProp } from "./components/popup/LookupPopup";
import Warning from "./components/popup/Warning";
import MaterialTable from "material-table";
import { exportExcel, readExcel } from "./utils";

const initLookupData: LookupPopupProp = {
  columns: undefined,
  data: undefined,
  okHandler: undefined,
  cancelHandler: undefined
};

const excelJson = [
  {
    name: "clientA",
    email: "clientA@gmail.com",
    tel: 22223333,
    birth: new Date()
  },
  {
    name: "clientB",
    email: "clientB@gmail.com",
    tel: 33334444,
    birth: new Date()
  },
  {
    name: "clientC",
    email: "clientC@gmail.com",
    tel: 44445555,
    birth: new Date()
  },
  {
    name: "clientD",
    email: "clientD@gmail.com",
    tel: 55556666,
    birth: new Date()
  }
];

const columns = [
  { field: "itemCode", title: "Item Code" },
  { field: "itemDesc", title: "Item Desc" }
];
const data = [
  { itemCode: "PARA01", itemDesc: "PARAPARAPAR01" },
  { itemCode: "PARA02", itemDesc: "PARARPARAPRARPA02" }
];

interface XlsxProps {
  columns: any;
  data: any[];
  fileName: string;
  sheetName?: string;
}

const MyForm = () => {
  const [lookupData, setLookupData] = useState(initLookupData);

  console.log("render MyForm");

  return (
    <Formik
      initialValues={{
        // textfield
        firstName: "",
        lastName: "",
        email: "",
        tel: "",
        addr: "",
        officeAddr: "",
        officeTel: "",
        test: "",
        // dropdown
        animals: "",
        // checkbox
        autoRepeat: true,
        // radio group
        gender: "O",
        // date picker
        date: new Date(),
        //item code lookup textfield
        itemCodes: "",
        itemDescs: ""
      }}
      validationSchema={Yup.object().shape({
        date: Yup.date().required(),
        firstName: Yup.string().required(),
        lastName: Yup.string().required(),
        email: Yup.string().required()
      })}
      validateOnChange={false}
      validate={values => {
        let errors: FormikErrors<typeof values> = {};
        if (values.firstName === "123") {
          errors.firstName = "firstName is '123'! Are you serious?";
        }
        if (!values.date) {
          errors.date = "date is a required field";
        } else if (!moment(values.date).isValid()) {
          errors.date = "Invalid date format";
        }
        return errors;
      }}
      onSubmit={(values, actions) => {
        setTimeout(() => {
          console.log(JSON.stringify(values, null, 2));
          actions.setSubmitting(false);
        }, 500);
      }}
    >
      {({ values, setFieldValue, isSubmitting, errors, setErrors }) => (
        <>
          <h3>Table</h3>
          <div id="excelContent" />
          <MaterialTable
            columns={[
              { field: "name", title: "Client Name" },
              { field: "email", title: "Email" },
              { field: "tel", title: "Tel" },
              { field: "birth", title: "Birthday", type: "date" }
            ]}
            data={excelJson}
            options={{
              draggable: false,
              paging: false,
              minBodyHeight: 100,
              maxBodyHeight: 100,
              tableLayout: "fixed",
              search: false,
              padding: "dense"
            }}
          />
          <div>
            <Button
              onClick={() =>
                exportExcel({
                  columns: [
                    { title: "Client Name", key: "name", width: 30 },
                    { title: "Email", key: "email", width: 40 },
                    { title: "Tel", key: "tel", width: 25 },
                    { title: "Birthday", key: "birth", width: 25 }
                  ],
                  data: excelJson.map(item => ({
                    name: item.name,
                    email: item.email,
                    tel: item.tel,
                    birth: item.birth
                  })),
                  fileName: "ccc"
                })
              }
            >
              Export Excel
            </Button>
            <Button component="label">
              Read Excel
              <input
                type="file"
                style={{ display: "none" }}
                onChange={async event => {
                  if (event.target.files && event.target.files[0]) {
                    const data = await readExcel(event.target.files[0]);
                    console.log(data);
                    if (data) {
                      document.getElementById(
                        "excelContent"
                      )!.innerHTML = JSON.stringify(data);
                    }
                  }
                }}
              />
            </Button>
          </div>
          <Form>
            <div>{JSON.stringify(values, null, 2)}</div>

            <div>
              <h3>Date Picker</h3>
              <FastField
                style={{ width: "150px" }}
                component={FormikDatePicker}
                name="date"
                label="Date"
              />
            </div>

            <div>
              <h3>Radio Group</h3>
              <FastField
                name="gender"
                legend="Gender"
                row={true}
                component={FormikRadioGroup}
              >
                <FormControlLabel
                  value="F"
                  control={<Radio />}
                  label="Female"
                />
                <FormControlLabel value="M" control={<Radio />} label="Male" />
                <FormControlLabel value="O" control={<Radio />} label="Other" />
                <FormControlLabel
                  value="D"
                  disabled
                  control={<Radio />}
                  label="(Disabled option)"
                />
              </FastField>
            </div>

            <div>
              <h3>Checkbox</h3>
              <FastField
                style={{ width: "200px" }}
                name="autoRepeat"
                label="autoRepeat"
                disabled={isSubmitting}
                component={FormikCheckbox}
              />
            </div>

            <div>
              <h3>Dropdown</h3>
              <FastField
                style={{ width: "200px" }}
                name="animals"
                label="animals"
                disabled={isSubmitting}
                component={FormikSelect}
              >
                <MenuItem value="dogs">Dogs</MenuItem>
                <MenuItem value="cats">Cats</MenuItem>
                <MenuItem value="rats">Rats</MenuItem>
                <MenuItem value="snakes">Snakes</MenuItem>
              </FastField>
            </div>

            <div>
              <h3>TextField</h3>
              <FastField
                style={{ width: "500px" }}
                uppercase
                name="firstName"
                label="firstName"
                disabled={isSubmitting}
                component={FormikTextField}
                lookupOnClick={() => {
                  setLookupData({
                    columns,
                    data,
                    okHandler: () => setLookupData(initLookupData),
                    cancelHandler: () => setLookupData(initLookupData)
                  });
                }}
              />
              <br />
              <FastField
                id="lastName"
                name="lastName"
                label="lastName"
                disabled={isSubmitting}
                component={FormikTextField}
                lookupOnClick={() => {
                  console.log("hi");
                }}
              />
              <br />
              <FastField
                id="email"
                name="email"
                label="email"
                disabled={isSubmitting}
                component={FormikTextField}
                lookupOnClick={() => {
                  console.log("hi");
                }}
              />
              <br />
              <FastField
                id="tel"
                name="tel"
                label="tel"
                disabled={isSubmitting}
                component={FormikTextField}
              />
              <br />
              <FastField
                id="addr"
                name="addr"
                label="addr"
                disabled={isSubmitting}
                component={FormikTextField}
                lookupOnClick={() => {
                  console.log("hi");
                }}
              />
              <br />
              <FastField
                id="officeAddr"
                name="officeAddr"
                label="officeAddr"
                disabled={isSubmitting}
                component={FormikTextField}
                lookupOnClick={() => {
                  console.log("hi");
                }}
              />
              <br />
              <FastField
                id="officeTel"
                name="officeTel"
                label="officeTel"
                disabled={isSubmitting}
                component={FormikTextField}
                lookupOnClick={() => {
                  console.log("hi");
                }}
              />
            </div>

            <div>
              <h3>Item Code Lookup TextField</h3>
              <FastField
                style={{ width: "200px" }}
                uppercase
                name="itemCodes"
                label="Item Codes"
                disabled={true}
                component={FormikTextField}
                lookupOnClick={() => {
                  setLookupData({
                    columns: [
                      { field: "itemCode", title: "Item Code" },
                      { field: "itemDesc", title: "Item Desc" }
                    ],
                    data: [
                      { itemCode: "PARA01", itemDesc: "PARAPARAPAR01" },
                      { itemCode: "PARA02", itemDesc: "PARARPARAPRARPA02" }
                    ],
                    okHandler: selected => {
                      setFieldValue("itemCodes", selected.itemCode);
                      setFieldValue("itemDescs", selected.itemDesc);
                      setLookupData(initLookupData);
                    },
                    cancelHandler: () => setLookupData(initLookupData)
                  });
                }}
              />
              <FastField
                style={{ width: "500px" }}
                name="itemDescs"
                disabled={true}
                component={FormikTextField}
              />
            </div>

            <Button type="submit" variant="contained" color="primary">
              Submit
            </Button>
          </Form>

          {!R.isEmpty(errors) && (
            <Warning errors={errors} setErrors={setErrors} />
          )}
          {lookupData.columns && <LookupPopup {...lookupData} />}
        </>
      )}
    </Formik>
  );
};

const App: React.FC = () => {
  console.log("render App");
  return (
    <div>
      <MyForm />
    </div>
  );
};

export default App;
