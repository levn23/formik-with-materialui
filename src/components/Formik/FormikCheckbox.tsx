import React from "react";
import { Checkbox, CheckboxProps, FormControlLabel } from "@material-ui/core";
import { FieldProps } from "formik";

interface FormikCheckboxProps extends FieldProps {
  style?: React.CSSProperties;
  label?: string;
}

const fieldToCheckbox = ({
  field: { name, value },
  form: { setFieldValue },
  ...props
}: FormikCheckboxProps): CheckboxProps => {
  return {
    ...props,
    name,
    checked: value,
    onChange: event => setFieldValue(name, event.target.checked)
  };
};

export default ({ style, label, ...props }: FormikCheckboxProps) => {
  return (
    <FormControlLabel
      style={style}
      label={label}
      control={<Checkbox {...fieldToCheckbox(props)} />}
    />
  );
};
